FROM python:3.6.15

WORKDIR /app

COPY . /app

RUN  pip install --no-cache gunicorn==20.1.0 \
&& pip install --no-cache -r requirements.txt 

EXPOSE 5000

ENTRYPOINT [ "/bin/sh", "/app/entrypoint.sh" ]


